import React from 'react';
import Modal from 'react-modal';
import { connect } from 'react-redux';
import { addNewTransaction } from '../../store/actions/transactionActions';

const customStyles = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
        width: '500px'
    }
};

class CreateTransaction extends React.Component {

    state = {
        amount: 0,
        type: '',
        note: '',
    }

    changeHandler = e => {
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    submitHandler = e => {
        e.preventDefault();
        this.props.addNewTransaction(this.state);
        this.setState({
            amount: 0,
            type: '',
            note: '',
        });
    }

    render() {
        let { amount, note } = this.state;
        return (
            <Modal
                isOpen={this.props.isOpen}
                onRequestClose={this.props.close}
                style={customStyles}
                contentLabel="Create a New Transaction"
            >
                <h2>Create a New Transaction</h2>
                <form onSubmit={this.submitHandler}>
                    <div className="form-group">
                        <label htmlFor="amount">Amount:</label>
                        <input
                            className="form-control"
                            type="number"
                            placeholder="Enter Amount"
                            name="amount"
                            id="amount"
                            value={amount}
                            onChange={this.changeHandler}
                        />
                    </div>
                    <div className="form-group">
                        <label htmlFor="type">Type:</label>
                        <select
                            className="form-control"
                            onChange={this.changeHandler}
                            name="type"
                        >
                            <option>Select Type</option>
                            <option value="expense">Expense</option>
                            <option value="income">Income</option>
                        </select>
                    </div>
                    <div className="form-group">
                        <label htmlFor="note">Amount:</label>
                        <textarea
                            className="form-control"
                            placeholder="Enter Short Note"
                            name="note"
                            id="note"
                            value={note}
                            onChange={this.changeHandler}
                        />
                    </div>
                    <div>
                        <button className="btn btn-primary">Submit</button>
                    </div>
                </form>
            </Modal>
        )
    }
}

export default connect(null, { addNewTransaction })(CreateTransaction);