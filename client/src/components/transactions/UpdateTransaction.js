import React from 'react';
import Modal from 'react-modal';
import { connect } from 'react-redux';
import { updateTransaction } from '../../store/actions/transactionActions';

const customStyles = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
        width: '500px'
    }
};

class UpdateTransaction extends React.Component {

    state = {
        amount: 0,
        note: ''
    }

    componentDidMount() {
        this.setState({
            amount: this.props.transaction.amount,
            note: this.props.transaction.note
        });
    }

    changeHandler = e => {
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    submitHandler = e => {
        e.preventDefault();
        this.props.updateTransaction(this.props.transaction._id, this.state);
        this.props.close();
    }

    render() {
        let { amount, note } = this.state;
        console.log('this props =>', this.props)
        return (
            <Modal
                isOpen={this.props.isOpen}
                onRequestClose={this.props.close}
                style={customStyles}
                contentLabel="Update a New Transaction"
            >
                <h2>Update a New Transaction</h2>
                <form onSubmit={this.submitHandler}>
                    <div className="form-group">
                        <label htmlFor="amount">Amount:</label>
                        <input
                            className="form-control"
                            type="number"
                            placeholder="Enter Amount"
                            name="amount"
                            id="amount"
                            value={amount}
                            onChange={this.changeHandler}
                        />
                    </div>
                    <div className="form-group">
                        <label htmlFor="note">Amount:</label>
                        <textarea
                            className="form-control"
                            placeholder="Enter Short Note"
                            name="note"
                            id="note"
                            value={note}
                            onChange={this.changeHandler}
                        />
                    </div>
                    <div>
                        <button className="btn btn-primary">Submit</button>
                    </div>
                </form>
            </Modal>
        )
    }
}

export default connect(null, { updateTransaction })(UpdateTransaction);