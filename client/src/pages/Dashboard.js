import React from 'react';
import { connect } from 'react-redux';
import {
    loadTransactions,
    removeTransaction
} from '../store/actions/transactionActions';
import CreateTransaction from '../components/transactions/CreateTransaction';
import UpdateTransaction from '../components/transactions/UpdateTransaction';

class Dashboard extends React.Component {

    state = {
        createModalOpen: false,
        updateModalOpen: false,
        id: ''
    }

    componentDidMount() {
        this.props.loadTransactions();
    }

    openCreateModal = () => {
        this.setState({
            createModalOpen: true
        })
    }

    closeCreateModal = () => {
        this.setState({
            createModalOpen: false
        })
    }

    openUpdateModal = id => {
        console.log('update model clicked');
        this.setState({
            updateModalOpen: true,
            id
        })
    }

    closeUpdateModal = () => {
        this.setState({
            updateModalOpen: false,
            id: ''
        })
    }

    render() {
        let { auth, transactions, removeTransaction } = this.props;
        console.log('trans =>', transactions);
        return (
            <div className="row">
                <div className="col-md-8 offset-md-2">
                    <h1>Welcom {auth.user.name}</h1>
                    <p>Your email id is {auth.user.email}</p>
                    <button
                        className="btn btn-primary"
                        onClick={this.openCreateModal}
                    >
                        Create New Transaction
                    </button>
                    <CreateTransaction
                        isOpen={this.state.createModalOpen}
                        close={this.closeCreateModal}
                    />
                    <br />
                    <h1>Transactions</h1>
                    {transactions.length > 0
                        ? (
                            <ul className="list-group">
                                {
                                    transactions.map(trans => (
                                        <li
                                            key={trans._id}
                                            className="list-group-item"
                                        >
                                            <p>Type: {trans.type}</p>
                                            <p>Amount: {trans.amount}</p>
                                            {
                                                this.state.id === trans._id
                                                    ? <UpdateTransaction
                                                        isOpen={this.state.updateModalOpen}
                                                        close={this.closeUpdateModal}
                                                        transaction={trans}
                                                    />
                                                    : null
                                            }
                                            <button
                                                className="btn btn-danger"
                                                onClick={() => removeTransaction(trans._id)}
                                            >
                                                Remove
                                    </button>
                                            <button
                                                className="btn btn-success"
                                                onClick={() => this.openUpdateModal(trans._id)}
                                            >
                                                Update
                                    </button>
                                        </li>
                                    ))
                                }
                            </ul>
                        ) : (
                            <p>There is no transaction</p>
                        )}
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    auth: state.auth,
    transactions: state.transactions
});

export default connect(mapStateToProps, {
    loadTransactions,
    removeTransaction
})(Dashboard);