import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { login } from '../store/actions/authActions';


class Login extends React.Component {
    // email, password

    state = {
        email: '',
        password: '',
        error: {}
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if (
            JSON.stringify(nextProps.auth.error)
            !==
            JSON.stringify(prevState.error)
        ) {
            return {
                error: nextProps.auth.error
            }
        }
        return null;
    }

    changeHandler = e => {
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    submitHandler = e => {
        e.preventDefault();
        this.props.login({
            email: this.state.email,
            password: this.state.password
        }, this.props.history);
    }

    render() {
        let { email, password, error } = this.state;

        return (
            <div className="row">
                <div className="col-sm-6 offset-sm-3">
                    <h1 className="text-center display-4">Login Here</h1>
                    <form onSubmit={this.submitHandler}>
                        <div className="form-group">
                            <label htmlFor="email">Email:</label>
                            <input
                                className={error.email ? "form-control is-invalid" : "form-control"}
                                type="email"
                                placeholder="Email ID"
                                name="email"
                                id="email"
                                value={email}
                                onChange={this.changeHandler}
                            />
                            {
                                error.email &&
                                <div className="invalid-feedback">
                                    {error.email}
                                </div>
                            }
                        </div>
                        <div className="form-group">
                            <label htmlFor="password">Password:</label>
                            <input
                                className={error.password ? "form-control is-invalid" : "form-control"}
                                type="password"
                                placeholder="Password"
                                name="password"
                                id="password"
                                value={password}
                                onChange={this.changeHandler}
                            />
                            {
                                error.password &&
                                <div className="invalid-feedback">
                                    {error.password}
                                </div>
                            }
                        </div>
                        <div className="form-group">
                            <Link to="/register">Don't have account ? Register here</Link>
                            <br />
                            <button type="submit" className="btn btn-primary">Login</button>
                        </div>
                    </form>
                </div>
            </div>
        )
    }
}


const mapStateToProps = state => ({
    auth: state.auth
});

export default connect(mapStateToProps, { login })(Login);