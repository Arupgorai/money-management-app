import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { register } from '../store/actions/authActions';


class Register extends React.Component {
    // name, email, password

    state = {
        name: '',
        email: '',
        password: '',
        error: {}
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if (
            JSON.stringify(nextProps.auth.error)
            !==
            JSON.stringify(prevState.error)
        ) {
            return {
                error: nextProps.auth.error
            }
        }
        return null;
    }

    changeHandler = e => {
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    submitHandler = e => {
        e.preventDefault();
        let { name, email, password } = this.state;
        this.props.register({ name, email, password }, this.props.history);
    }

    render() {
        let { name, email, password, error } = this.state;

        return (
            <div className="row">
                <div className="col-sm-6 offset-sm-3">
                    <h1 className="text-center display-4">Register Here</h1>
                    <form onSubmit={this.submitHandler}>
                        <div className="form-group">
                            <label htmlFor="name">Name:</label>
                            <input
                                className={error.name ? "form-control is-invalid" : "form-control"}
                                type="text"
                                placeholder="Full Name"
                                name="name"
                                id="name"
                                value={name}
                                onChange={this.changeHandler}
                            />
                            {
                                error.name &&
                                <div className="invalid-feedback">
                                    {error.name}
                                </div>
                            }
                        </div>
                        <div className="form-group">
                            <label htmlFor="email">Email:</label>
                            <input
                                className={error.email ? "form-control is-invalid" : "form-control"}
                                type="email"
                                placeholder="Email ID"
                                name="email"
                                id="email"
                                value={email}
                                onChange={this.changeHandler}
                            />
                            {
                                error.email &&
                                <div className="invalid-feedback">
                                    {error.email}
                                </div>
                            }
                        </div>
                        <div className="form-group">
                            <label htmlFor="password">Password:</label>
                            <input
                                className={error.password ? "form-control is-invalid" : "form-control"}
                                type="password"
                                placeholder="Password"
                                name="password"
                                id="password"
                                value={password}
                                onChange={this.changeHandler}
                            />
                            {
                                error.password &&
                                <div className="invalid-feedback">
                                    {error.password}
                                </div>
                            }
                        </div>
                        <div className="form-group">
                            <Link to="/login">Already have account ? Login here</Link>
                            <br />
                            <button className="btn btn-primary">Register</button>
                        </div>
                    </form>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    auth: state.auth
});

export default connect(mapStateToProps, { register })(Register);