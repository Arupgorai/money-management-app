import Axios from 'axios';
import jwtDecode from 'jwt-decode'
import * as Types from './types';
import setAuthToken from '../../utils/setAuthToken';

// Register Action
export const register = (user, history) => {
    return dispatch => {
        Axios
            .post('/api/user/register', user)
            .then((res) => {
                dispatch({
                    type: Types.USERS_ERROR,
                    payload: {
                        error: {}
                    }
                });
                console.log(res);
                history.push('/login');
            })
            .catch(error => {
                dispatch({
                    type: Types.USERS_ERROR,
                    payload: {
                        error: error.response.data
                    }
                });
            });
    }
}

// Login Action
export const login = (user, history) => dispatch => {
    Axios
        .post('/api/user/login', user)
        .then(res => {
            console.log("login data =>", res);
            let token = res.data.token;
            console.log("token =>", token);

            // decode token
            let decode = jwtDecode(token);
            console.log("decode =>", decode);

            // save token to localStorage
            localStorage.setItem('auth_token', token);

            // set Auth Header
            setAuthToken(token);

            // Dispatch Set-User
            dispatch({
                type: Types.SET_USER,
                payload: {
                    user: decode
                }
            });

            // Redirect to home-page
            history.push('/');
        })
        .catch(error => {
            console.log(error.response.data);
            dispatch({
                type: Types.USERS_ERROR,
                payload: {
                    error: error.response.data
                }
            });
        });
}


// logout action
export const logout = history => dispatch => {
    localStorage.removeItem('auth-token');
    console.log('localstorage Deleted');
    history.push('/login');
    dispatch({
        type: Types.SET_USER,
        payload: {
            user: {}
        }
    });
}