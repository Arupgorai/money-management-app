import Axios from 'axios';
import * as Types from '../actions/types';

export const loadTransactions = () => dispatch => {
    Axios
        .get('/api/transactions')
        .then(response => {
            dispatch({
                type: Types.LOAD_TRANSACTIONS,
                payload: {
                    transactions: response.data
                }
            });
        })
        .catch(error => {
            console.log(error);
        })
}


// Add transaction action
export const addNewTransaction = transaction => dispatch => {
    Axios
        .post('/api/transactions', transaction)
        .then(response => {
            dispatch({
                type: Types.CREATE_TRANSACTION,
                payload: {
                    transaction: response.data
                }
            });
        })
        .catch(err => {
            console.log(err);
        });
}

// remove transaction action
export const removeTransaction = id => dispatch => {
    Axios
        .delete(`/api/transactions/${id}`)
        .then(resp => {
            dispatch({
                type: Types.REMOVE_TRANSACTION,
                payload: {
                    id: resp.data._id
                }
            });
        })
        .catch(err => {
            console.log(err);
        })
}

// update transaction
export const updateTransaction = (id, transaction) => dispatch => {
    Axios
        .put(`/api/transactions/${id}`, transaction)
        .then(resp => {
            dispatch({
                type: Types.UPDATE_TRANSACTION,
                payload: {
                    transaction: resp.data.transaction
                }
            });
        })
        .catch(err => console.log(err));
}