import Axios from 'axios';

const setAuthToken = token => {
    if (!token) {
        Axios.defaults.headers.common['Authorization'] = '';
    }

    Axios.defaults.headers.common['Authorization'] = token;
}

export default setAuthToken;