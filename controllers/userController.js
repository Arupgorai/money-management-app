const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const registerValidator = require('../validators/registerValidator');
const loginValidator = require('../validators/loginValidator');
const User = require('../model/User');
const { serverError, resourceError } = require('../util/error');


module.exports = {
    login(req, res) {
        // Extract user data
        let { email, password } = req.body;

        // validate data
        let validate = loginValidator({ email, password });

        if (!validate.isValid) {
            return res.status(400).json(validate.error);
        }

        // Check for user availability
        User
            .findOne({ email })
            // Use populate for transactions
            .then(user => {
                if (!user) {
                    return resourceError(res, 'User Not Found !!');
                }

                // Compare Password
                bcrypt.compare(password, user.password, (err, result) => {
                    if (err) {
                        return serverError(res, err);
                    }

                    if (!result) {
                        return resourceError(res, 'Password Does not match !!');
                    }

                    // Generate token and Response Back
                    let token = jwt.sign({
                        _id: user._id,
                        name: user.name,
                        email: user.email,
                        amount: user.amount,
                        income: user.income,
                        expense: user.expense,
                        transactions: user.transactions
                    }, 'SECRET_KEY', { expiresIn: '2h' });

                    res.status(200).json({
                        message: 'Login Successful',
                        token: `Bearer ${token}`
                    });
                });
            })
            .catch(err => serverError(res, err));
    },

    register(req, res) {

        // Read Client Data
        let { name, email, password } = req.body;

        // Validation check user data
        let validate = registerValidator({ name, email, password });

        if (!validate.isValid) {
            return res.status(400).json(validate.error);
        }

        // Check for duplicate user
        User
            .findOne({ email })
            .then(user => {
                if (user) {
                    return resourceError(res, 'Email Already Exist !!');
                }

                // hash passpord
                bcrypt.hash(password, 11, (err, hash) => {
                    if (err) {
                        return serverError(res, error);
                    }

                    // New user object
                    const user = new User({
                        name,
                        email,
                        password: hash,
                        balance: 0,
                        income: 0,
                        expense: 0,
                        transactions: []
                    });

                    // Save to Database
                    user
                        .save()
                        .then(user => {
                            res.status(201).json({
                                message: 'User created successfully',
                                user
                            });
                        })
                        .catch(err => serverError(res, err));
                });
            })
            .catch(err => serverError(res, err));
    },
    allUser(req, res) {
        User
            .find()
            .then(users => {
                if (!users) {
                    res.status(404).json({
                        message: "No User Found"
                    });
                }
                res.status(200).json(users);
            })
            .catch(err => serverError(res, err));
    }
}