const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const TransactionSchema = new Schema({
    amount: {
        type: Number,
        required: true
    },
    type: {
        type: String,
        required: true
    },
    note: String,
    author: {
        type: Schema.Types.ObjectId,
        ref: 'MMA_User'
    }
}, { timestamps: true });

const Transaction = mongoose.model('MMA_Transaction', TransactionSchema);

module.exports = Transaction;