const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
    name: {
        type: String,
        required: true,
        trim: true
    },
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    balance: Number,
    income: Number,
    expense: Number,
    transactions: {
        type: [
            {
                type: Schema.Types.ObjectId,
                ref: 'MMA_Transaction'
            }
        ]
    }
});


const User = mongoose.model('MMA_User', UserSchema);
module.exports = User;