const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const User = require('./model/User');

const opts = {};
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opts.secretOrKey = 'SECRET_KEY';


module.exports = passport => {
    passport.use(new JwtStrategy(opts, (payload, done) => {
        User
            .findOne({ _id: payload._id })
            .then(usr => {
                if (!usr) {
                    return done(null, false);
                }

                return done(null, usr);
            })
            .catch(err => {
                console.log(err);
                return done(err);
            });
    }));
}