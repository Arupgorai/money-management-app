const router = require('express').Router();
const { register, login, allUser } = require('../controllers/userController');

// Registration Route
// localhost:4000/api/user/register
router.post('/register', register);

// Login Route
// localhost:4000/api/user/login
router.post('/login', login);

router.get('/all', allUser);

module.exports = router;