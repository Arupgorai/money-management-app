const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const passport = require('passport');
const path = require('path');

const app = express();

// middleware
app.use(morgan('dev'));
app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(passport.initialize());
require('./passport')(passport);

app.use('/api/user', require('./routers/userRoute'));
app.use('/api/transactions', require('./routers/transactionRoute'));

if (process.env.NODE_ENV === 'production') {
    app.use(express.static('client/build'));
    app.get('*', (req, res) => {
        res.sendFile(path.resolve('__dirname', 'client', 'build', 'index.html'));
    });
}


app.get('/', (req, res) => {
    res.send("Welcome to Money Management App");
});

const PORT = process.env.PORT || 4000;
app.listen(PORT, () => {
    console.log(`server listening on port => ${PORT}`);
    mongoose.connect(`mongodb://${'arupuser'}:${'Aruppass123'}@ds351628.mlab.com:51628/globaldb`, {
        useUnifiedTopology: true,
        useNewUrlParser: true
    }, () => {
        console.log('mongodb connected [mLAB]');
    });
});