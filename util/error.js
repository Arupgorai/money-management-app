module.exports = {
    serverError(res, error) {
        console.log("Err =>", error);
        res.status(500).json({
            message: 'Server Error Occurred !!'
        });
    },
    resourceError(res, msg) {
        res.status(400).json({
            message: msg
        });
    }
}