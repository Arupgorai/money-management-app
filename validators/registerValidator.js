const validator = require('validator');

const validate = user => {
    let error = {};

    if (!user.name) {
        error.name = 'Please provide your name.'
    }

    if (!user.email) {
        error.email = 'Please provide your email.'
    } else if (!validator.isEmail(user.email)) {
        error.email = 'Please provide valid email.'
    }

    if (!user.password) {
        error.password = 'Please provide password.'
    } else if (user.password.length < 6) {
        error.password = 'Password must be greater or equal to 6 characters.'
    }

    return {
        error,
        isValid: Object.keys(error).length === 0
    }
};

module.exports = validate;